package Steps;

import Base.BaseUtil;
import Pages.LoginPage;
import com.sun.tools.internal.xjc.reader.xmlschema.bindinfo.BIConversion;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.openqa.selenium.By;
import java.util.ArrayList;
import java.util.List;
import static org.testng.AssertJUnit.assertEquals;

public class LoginStep extends BaseUtil {

    private BaseUtil baseUtil;

    public LoginStep(BaseUtil baseUtil) {
        this.baseUtil = baseUtil;
    }

    @Given("^I navigate to the login page$")
    public void iNavigateToTheLoginPage() {
        System.out.println("Navigate Login Page");
        baseUtil.driver.navigate().to("http://executeautomation.com/demosite/Login.html");
    }

    @And("^I enter the following for Login$")
    public void iEnterTheFollowingForLogin(DataTable table)  {

        List<User> users = new ArrayList<User>();
        // store all the users
        users = table.asList(User.class);

        LoginPage page = new LoginPage(baseUtil.driver);

        for (User user : users) {
            page.Login(user.usernamed, user.passwordd);
        }
    }

    public class User {
        public String usernamed;
        public String passwordd;

        public User(String userName, String passWord) {
            usernamed = userName;
            passwordd = passWord;
        }
    }

    @And("^I click login button$")
    public void iClickLiginButton() {
        LoginPage page = new LoginPage(baseUtil.driver);
        page.ClickLogin();
    }

    @Then("^I should see the userform page$")
    public void iShouldSeeTheUserformPage() {

        assertEquals("Its not displayed", baseUtil.driver.findElement(By.id("Initial")).isDisplayed(), true);
    }
}
