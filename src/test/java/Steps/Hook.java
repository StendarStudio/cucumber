package Steps;

import Base.BaseUtil;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.chrome.ChromeDriver;

public class Hook extends BaseUtil {

    private BaseUtil baseUtil;

    public Hook(BaseUtil baseUtil) {
        this.baseUtil = baseUtil;
    }

    @Before
    public void InitializeTest() {
        System.out.println("Opening the browser: Chrome");

        System.setProperty("webdriver.chrome.driver", "C:/drivers/chromedriver.exe");
        baseUtil.driver = new ChromeDriver();
    }

    @After
    public void TearDownTest(Scenario scenario) {

        if (scenario.isFailed()) {
            // take screenshot
            System.out.println(scenario.getName());
        }

        baseUtil.driver.close();
        baseUtil.driver.quit();

        System.out.println("Closing the browser: Chrome");
    }
}
