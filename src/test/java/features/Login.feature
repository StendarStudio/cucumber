Feature: Loginfeature
  This feature deals with the login functionality of the application.

  Scenario: Login with correct username and password using Scenario outline
    Given I navigate to the login page
    And I enter the following for Login
      | usernamed | passwordd |
      | admin     | password  |
    And I click login button
    Then I should see the userform page

